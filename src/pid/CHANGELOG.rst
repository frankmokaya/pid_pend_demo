^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package pid
^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.3 (2015-03-14)
------------------

0.0.2 (2015-03-13)
------------------

0.0.1 (2015-03-08)
------------------
* Fixing various minor bugs related to user input.
* Pre-release commit.
* It WORKS!
* It's talking with the plant sim now. Just the PID programming remains.
* Making progress with the command line input.
* Rough outline of the program. Need to take inputs on the command line next.
* Initial commit
* Contributors: Andy Zelenak
